<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('usuarios', 'UsuarioController@Index')->name('users');

Route::get('usuarios/{usuario}', 'UsuarioController@Detalles')->where('usuario','[0-9]+')->name('users.detalle');

Route::get('usuarios/nuevo', 'UsuarioController@Crear')->name('users.crear');

Route::post('usuarios/crear', 'UsuarioController@Store')->name('users.store');

Route::get('usuarios/editar/{usuario}', 'UsuarioController@Editar')->name('users.edit');

// Route::post('usuarios/update', 'UsuarioController@Update')->name('users.update');
Route::put('usuarios/{usuario}', 'UsuarioController@Update')->name('users.update');

Route::delete('usuarios/{usuario}', 'UsuarioController@Destroy')->name('users.destroy');

<?php

use App\Models\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // DB::insert('INSERT INTO professions (title) VALUES(:title)', ["title" => "Diseñador"]);
        // DB::insert('INSERT INTO professions (title) VALUES(?)', ["dseafea"]); 
     //    DB::table('professions')->insert([
     //        'title' => 'Desarrollador Back-end',
     //    ]);
     //    DB::table('professions')->insert([
     //        'title' => 'Desarrollador Front-end',
     //    ]);
     //    DB::table('professions')->insert([
     //        'title' => 'Diseñador web',
     //    ]);
        DB::table('professions')->insert([
            'title' => 'Diseñador',
        ]);
        DB::table('professions')->insert([
            'title' => 'Desarrollador Front-end',
        ]);
        DB::insert('INSERT INTO professions (title) VALUES(?)', ["dseafea"]); 

        Profession::create([
            'title' => 'Desarrollador back-end model',
        ]);
        factory(Profession::class, 17)->create();
    }
}

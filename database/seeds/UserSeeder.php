<?php

use App\User;
use App\Models\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $professiones = DB::select('SELECT Id FROM professions WHERE title = ? LIMIT 0,1'["dfsa"]);
        // $professions = DB::table('professions')->select('id')->take(1)->get();
        // $profession = DB::table('professions')->select('id')->first();

        // $professionId = DB::table('professions')
        //             ->where(['title' =>'Diseñador'])
        //             ->value('id');
        // $professionId = DB::table('professions')
        //             ->where('title' , 'Diseñador')
        //             ->value('id');
        $professionId = DB::table('professions')
                    ->whereTitle('Diseñador')
                    ->value('id');
        //dd($profession->id);


        
        // DB::table('users')->insert([
        //     'name' => 'Daniel Forero',
        //     'email' => 'Daniel.Forero93@gmail.com',
        //     'password' => bcrypt('Laravel'),
        //     'profession_id' => $professionId,
        // ]);

        // $professions = Profession::where('title', 'Desarrollador back-end model')->value('id');   

        factory(User::class)->create([
            'email' => 'Daniel.Forero93@gmail.com',
            'password' => bcrypt('Laravel'),
            'profession_id' => $professionId,
            'is_admin' => true,
        ]);
        factory(User::class)->create([
            'profession_id' => $professionId
        ]);

        factory(User::class, 48)->create();
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    public function Index()
    {
        // $usuarios = DB::table('users')->get();
    	$usuarios = User::all();
        $title = "Usuarios";

         return view('usuarios.index')->with('usuarios', $usuarios);
    	//return view('usuarios.index')->compact('title','usuarios');
    }

    // public function Detalles($id)
    // {
    //     $usuario = User::findOrFail($id);
    //     // if($usuario == null){
    //     //     return response()->view('errors.404', [], 404);
    //     // }
    // 	return view('usuarios.detalle')->with('usuario', $usuario);
    // }
    public function Detalles(User $usuario)
    {
        //$usuario = User::findOrFail($id);
        // if($usuario == null){
        //     return response()->view('errors.404', [], 404);
        // }
        return view('usuarios.detalle')->with('usuario', $usuario);
    }
    public function Crear()
    {
    	return view('usuarios.crear');
    }
    public function Store()
    {
        // $data = request()->all();
        $data = request()->validate(
            [
                'name' => 'required',
                'email' => ['required', 'email', 'unique:users,email'],
                'password' => 'min:6'
            ],
            [
                'name.required' => 'El campo nombre es requerido',
                'password.min' => 'El campo password debe tener minimo 6 caracteres'
            ]
        );
        
        User::create([
            'name' => $data["name"],
            'email' => $data["email"],
            'password' =>  bcrypt(123456)
        ]);
        // return redirect("usuarios");
        return redirect()->route("users");
    }

    public function Editar(User $usuario)
    {
        return view('usuarios.editar')->with('usuario', $usuario);
    }

    public function Update(User $usuario)
    {
        // dd($usuario->id);
        // $data = request()->validate([
        //     'name' => 'required',
        //     'email' => ['required', 'email', 'unique:users,email,'.$usuario->id],
        //     'password' => ''
        // ]);
        $data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($usuario->id)],
            'password' => ''
        ]);
        if ($data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        }else{
            unset($data['password']);
        }
        $usuario->update($data);
        // dd($usuario);
        return redirect()->route('users.detalle', ['usuario' => $usuario]);
        // return redirect('usuarios.detalle')->with('usuario', $usuario);
    }
    public function Destroy(User $usuario){
        $usuario->delete();
        return redirect()->route('users');
    }

}

<?php

namespace App;

use App\Models\Profession;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    
    protected $casts = [
        'is_admin' => 'boolean'
    ];

    // para q no se pueda cargar de forma masiva
    protected $guarded = ['is_admin'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function findByEmail($email)
    {
        return static::where('email', $email)->first();
    }
    public function  profession(){
        return $this->belongsTo(Profession::class);
    }
    public function isAdmin(){
        return $this->is_admin;
    }
}

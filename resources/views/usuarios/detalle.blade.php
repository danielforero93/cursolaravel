@extends('layout')

@section('title', "Usuario {$usuario->id}")

@section('content')
	<h1>Detalles usuario {{$usuario->id}}</h1>
	<p>Nombre del usuario: {{$usuario->name}}</p>
	<p>Correo del usuario: {{$usuario->email}}</p>
	<p>
		 <a href="{{ url('/usuarios') }}">Regresar a la lista de usuarios</a>	

		{{-- <a href="{{ action('UsuarioController@index') }}">Regresar a la lista de usuarios</a>	 --}}
		{{--  <a href="{{ url()->previous() }}">Regresar a la lista de usuarios</a>	 --}}
	</p>
@endsection
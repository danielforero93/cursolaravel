@extends('layout')
@section('title', "Crear usuario")
@section('content')
	<div class="card">
		<h3 class="card-header">
			Crear Usuario
		</div>
		<div class="card-body">
			@if ($errors->any())
				<div class="alert alert-danger">
					<p>Por favor corrige los errores</p>
					<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach	
					</ul>
				</div>
			@endif
			<form method="POST" action="{{ url('usuarios/crear') }}">
				{{ csrf_field() }}
				<div class="form-group">
				    <label for="name">Nombre</label>
				    <input class="form-control" type="text" id="name" name="name" placeholder="Name">
				</div>
				<div class="form-group">
				    <label for="email">Email</label>
				    <input class="form-control" type="email" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
				</div>

				<div class="form-group">
				    <label for="email">Password</label>
				    <input class="form-control" type="password" id="password" name="password" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-primary">Crear Usuario</button>
				<a class="btn btn-link" href="{{ route('users')}}">Regresar al listado</a>


				{{-- <div class="row">
					<div class="col-4"><span>Nombres:</span></div>
					<div class="col-8">
						<input type="text" id="name" name="name" placeholder="Name"/>
						@if ($errors->has('name'))
							<p>{{ $errors->first('name')}}</p>
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-4"><span>email:</span></div>
					<div class="col-8">
						<input type="email" id="email" name="email" placeholder="Email" value="{{ old('email') }}" />
					</div>
				</div>

				<div class="row">
					<div class="col-4"><span>password:</span></div>
					<div class="col-8">
						<input type="password" id="password" name="password" placeholder="Password"/>
						@if ($errors->has('password'))
							<p>{{ $errors->first('password')}}</p>
						@endif
					</div>
				</div> --}}
			</form>
		</div>
	</div>
	
@endsection
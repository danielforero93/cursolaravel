@extends('layout')
@section('title', "Editar usuario")
@section('content')
	<h1>Editar Usuario</h1>
	@if ($errors->any())
		<div class="alert alert-danger">
			<p>Por favor corrige los errores</p>
			<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach	
			</ul>
		</div>
	@endif
	<form method="POST" action="{{ url("usuarios/{$usuario->id}") }}">
		{{ csrf_field() }}
		{{ method_field('PUT')}}
		<div class="row">
			<div class="col-4"><span>Nombres:</span></div>
			<div class="col-8">
				<input type="text" id="name" name="name" placeholder="Name" value="{{ old('name', $usuario->name)}}" />
				@if ($errors->has('name'))
					<p>{{ $errors->first('name')}}</p>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-4"><span>email:</span></div>
			<div class="col-8">
				<input type="email" id="email" name="email" placeholder="Email" value="{{ old('email', $usuario->email) }}" />
			</div>
		</div>

		<div class="row">
			<div class="col-4"><span>password:</span></div>
			<div class="col-8">
				<input type="password" id="password" name="password" placeholder="Password"/>
				@if ($errors->has('password'))
					<p>{{ $errors->first('password')}}</p>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-4"></div>
			<div class="col-4">
				<button >Actualizar Usuario</button>
				{{-- <input text="CREAR" type="submit" /> --}}
			</div>
		</div>
	</form>
	<a href="{{ route('users')}}">Volver</a>
@endsection
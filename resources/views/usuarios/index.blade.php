@extends('layout')

@section('title', "Usuarios")

@section('content')
<div class="d-flex justify-content-between align-items-end mb-3">
	<h1 class="pb-1">Usuarios Registrados</h1>
	<p>
		<a class="btn btn-primary" href="{{ route('users.crear') }}">Nuevo Usuario</a>
	</p>
</div>
	
	@if ($usuarios->isNotEmpty())
		<table class="table">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Email</th>
		      <th scope="col">Opciones</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach ($usuarios as $user)
		  		<tr>
			      <th scope="row">{{$user->id}}</th>
			      <td>{{$user->name}}</td>
			      <td>{{$user->email}}</td>
			      <td>
					<form method="POST" action="{{ route("users.destroy" , $user)}}">
						{{csrf_field()}}
						{{ method_field("DELETE")}}
				      	<a class="btn btn-link" class="" href="{{ route ('users.detalle', $user) }}"><span class="oi oi-eye"></span> </a>
						<a class="btn btn-link" href="{{ route("users.edit" , $user)}}"><span class="oi oi-pencil"></span> </a>
						<button class="btn btn-link" type="submit"><span class="oi oi-delete"></span></button>
					</form>

			      </td>
			    </tr>
		  	@endforeach
		  </tbody>
		</table>
	@else
		<p>No hay usuarios</p>
	@endif
	
	{{-- <ul>
		@forelse($usuarios as $user)
			<li>
				{{ $user->name }} ({{ $user->email }}) --}}
				{{-- <a href="{{ url("/usuarios/{$user->id}") }}"> Ver Detalles</a> --}}
				{{-- <a href="{{ action('UsuarioController@Detalles', ['id' =>$user->id]) }}">ver</a> --}}
				{{-- <a href="{{ route ('users.detalle', ['id' => $user]) }}">Ver</a> | 
				<a href="{{ route("users.edit" , ['id' => $user])}}">Editar</a>
				<a href="{{ route ('users.detalle', $user) }}">Ver</a> | 
				<a href="{{ route("users.edit" , $user)}}">Editar</a> |
				<form method="POST" action="{{ route("users.destroy" , $user)}}">
					{{csrf_field()}}
					{{ method_field("DELETE")}}
					<button type="submit">Eliminar</button>
				</form>
			</li>
		@empty
			<p>No hay ususarios registrados</p>
		@endforelse
	</ul> --}}
@endsection